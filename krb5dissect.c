/* krb5dissect.c --- Dissect Kerberos ccache/keytab files.
 * Copyright (C) 2007 Simon Josefsson.
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this file; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/* Autoconf definitions. */
#include <config.h>

/* Standard headers. */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

/* Gnulib headers. */
#include "progname.h"
#include "version-etc.h"
#include "read-file.h"
#include "error.h"
#include "xvasprintf.h"

/* Our headers. */
#include "ccache.h"
#include "keytab.h"
#include "krb5dissect_cmd.h"

const char version_etc_copyright[] = "Copyright %s %d Simon Josefsson.";

void
parse_ccache (const char *data, size_t len)
{
  struct ccache ccache;
  struct ccache_credential cred;
  int rc;
  size_t i = 0;

  rc = ccache_parse (data, len, &ccache);
  if (rc < 0)
    error (EXIT_FAILURE, 0, "Cannot parse ccache");

  ccache_print (&ccache);

  while (ccache.credentialslen)
    {
      size_t n;

      rc = ccache_parse_credential (ccache.credentials,
				    ccache.credentialslen, &cred, &n);
      if (rc < 0)
	error (EXIT_FAILURE, 0, "Cannot parse ccache entry %d", i);

      printf ("\nCcache entry %d:\n", i++);

      ccache_print_credential (&cred);

      ccache.credentials += n;
      ccache.credentialslen -= n;
    }
}

void
parse_keytab (const char *data, size_t len)
{
  struct keytab keytab;
  struct keytab_entry entry;
  int rc;
  size_t i = 0;

  rc = keytab_parse (data, len, &keytab);
  if (rc < 0)
    error (EXIT_FAILURE, 0, "Cannot parse keytab");

  keytab_print (&keytab);

  while (keytab.keytabslen)
    {
      size_t n;

      rc = keytab_parse_entry (keytab.keytabs,
			       keytab.keytabslen, &entry, &n);
      if (rc < 0)
	error (EXIT_FAILURE, 0, "Cannot parse keytab entry %d", i);

      printf ("\nKeytab entry %d:\n", i++);

      keytab_print_entry (&entry);

      keytab.keytabs += n;
      keytab.keytabslen -= n;
    }
}

int
main (int argc, char *argv[])
{
  struct gengetopt_args_info args_info;
  int free_filename = 0;
  char *filename;
  size_t len;
  char *data;

  set_program_name (argv[0]);

  if (cmdline_parser (argc, argv, &args_info) != 0)
    return EXIT_FAILURE;

  if (!args_info.quiet_given)
    {
      version_etc (stdout, NULL, PACKAGE, VERSION,
		   "Simon Josefsson", (char*) NULL);
      puts("");
    }

  if (args_info.keytab_given)
    filename = "/etc/krb5.keytab";
  else if (args_info.ccache_given)
    {
      filename = xasprintf("/tmp/krb5cc_%u", (unsigned long) getuid ());
      free_filename = 1;
    }
  else if (args_info.inputs_num == 1)
    filename = args_info.inputs[0];
  else
    {
      cmdline_parser_print_help ();
      return EXIT_FAILURE;
    }

  cmdline_parser_free (&args_info);

  if (strcmp (filename, "-") == 0)
    data = fread_file (stdin, &len);
  else
    data = read_binary_file (filename, &len);
  if (!data)
    error (EXIT_FAILURE, errno, "%s", filename);

  if (free_filename)
    free (filename);

  if (len > 2 && data[0] == '\x05' && data[1] == '\x04')
    parse_ccache (data, len);
  else
    parse_keytab (data, len);

  free (data);

  return 0;
}
